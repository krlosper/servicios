const express= require('express');
const app = express();
const puerto=3000;
const fs = require('fs');
const bodyParser=require('body-parser');
const jwt = require('jsonwebtoken');
const multipart =require('connect-multiparty');
const cors=require('cors');
app.use(cors());

const multiPartMiddleware =multipart({
    uploadDir:'./subidas'
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
extended:true   
}));

app.post('/api/subir/:id',multiPartMiddleware,(req,res)=>{   
    fs.rename(__dirname+"\\"+req.files.uploads[0].path, __dirname+"/subidas/"+req.params.id+"_"+req.files.uploads[0].name, function (err) {
      if (err) throw err;
      console.log('Nombre Editado');
    });
    console.log()
  res.json({
      'message':'guardado correctamente',
      'DirDocu':req.files.uploads[0].path
  });
  });
app.get('/api/Archivos',(req,res)=>{
  fs.readdir(__dirname+"/subidas/",  
      { withFileTypes: true }, 
      (err, files) => { 
      if (err) 
        console.log(err); 
      else { 
          res.json(files);
      } 

    }) 
});
app.get('/api/descargas/:id', function(req, res) {
 // res.send('valors: ' + req.params.id);    
  var file = __dirname +'/subidas/'+req.params.id; 
 res.download(file); 
});
const db = new (require('rest-mssql-nodejs'))({
    user: 'sa',
    password: 'desa',
    server: 'localhost', // replace this with your IP Server
    database: 'KnowlegdeDB',
    port: 1433, // this is optional, by default takes the port 1433
    options: { 
        encrypt: true, // this is optional, by default is false
        enableArithAbort: true
    } 
});

app.get('/api/vista',(req,res)=>{
    setTimeout(async()=>{
        const resultado= await db.executeQuery('select * from ErroresBase');
        res.json(resultado.data[0]);
        }, 1500);

});
app.get('/api/Aplicativo',(req,res)=>{
    setTimeout(async()=>{
        const resultado= await db.executeQuery('select * from Aplicativo');
        res.json(resultado.data[0]);
        }, 1500);

});
app.get('/api/Asignado',(req,res)=>{
    setTimeout(async()=>{
        const resultado= await db.executeQuery('select * from Asignado');
        res.json(resultado.data[0]);
        }, 1500);

});
app.get('/api/Tecnologia',(req,res)=>{
    setTimeout(async()=>{
        const resultado= await db.executeQuery('select * from Tecnologia');
        res.json(resultado.data[0]);
        }, 1500);

});
app.get('/api/logeo',(req,res)=>{
    setTimeout(async()=>{
        const resultado= await db.executeQuery('select * from Logeo');
        res.json(resultado.data[0]);
        }, 1500);

});
function CrearToken(datos){
 const usuario = datos;
    const token1 = jwt.sign({usuario}, "KrlitosPTE",{expiresIn:'700s'});
   // jwt.verify(token1,"clave", (err, decoded)=> {
   //     console.log(decoded.usuario.id,decoded.usuario.nombre); 
    //    console.log(err); // bar
    //  });
return token1;
}

  app.post('/api/Registar',(req,res,next)=>{  
       setTimeout(async()=>{
        const resultado= await db.executeQuery("SELECT * FROM LOGEO where Usuario='"+req.body.Usuario+"' and Contraseña='"+req.body.Contraseña+"';");
       // res.json(resultado.data[0]);
       if(resultado.data[0].length>0 && resultado.data[0][0].Contraseña==req.body.Contraseña){
   if(resultado.data[0][0].Estado==1)
   {
      const newtoken =  CrearToken(resultado.data[0]);
      res.json({
           Ingreso:true,
          token:newtoken});
   }else
   {
       res.json( {
           Ingreso:false,
           Mensajes:"Usuario Bloqueado"});
   }
       }else{
res.json( {
    Ingreso:false,
    Mensajes:"Usuario o Contraseña Incorrectos"}); 
       }

        }, 1500); 
  });

  app.get('/api/Acceso',verificartoken,(req,res)=>{  
    jwt.verify(req.token,'KrlitosPTE',(error,authData)=>{
       if(error){
  res.json( {
      Ingreso:false,
      Mensajes:"Token Incorrecto"}); 
       }else{
res.json({
Ingreso:true,
Mensajes:"Acceso correcto",
authData
});
       }
    })
  });

function verificartoken(req,res,next){
const bearerheader =req.headers['authorization'];
if(typeof bearerheader!=='undefined'){
const bearerToken=bearerheader.split(" ")[1];
req.token=bearerToken;
next();
}else{
res.json( {Mensajes:"Estructura Incorrecta"}); 
}
  }

    app.post('/api/Values',(req,res,next)=>{  
       setTimeout(async()=>{
        const resultado= await db.executeQuery("INSERT INTO ErroresDB (FechaReporte,idTecnologia,idAplicativo,idAsignado,idServicio,DescripcionError,DescripcionSolucion,Observaciones,docs) values('"+req.body.fechaReporte+"',"+req.body.idTecnologia+","+req.body.idAplicativo+","+req.body.idAsignado+",'"+req.body.idServicio+"','"+req.body.descripcionError+"','"+req.body.descripcionSolucion+"','"+req.body.observaciones+"','"+req.body.docs+"');");
        res.json({
            Mensajes:"Insercion exitosa"
        })
        }, 1500); 
  });


app.listen(puerto,()=>console.log('port 3000'));
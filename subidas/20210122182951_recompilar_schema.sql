select 'ALTER PACKAGE ADM_COMEX.' || OBJECT_NAME || ' COMPILE;' 
from   user_objects 
where  status = 'INVALID'
and    object_type in ('PACKAGE','PACKAGE BODY');

Begin
DBMS_UTILITY.compile_schema(schema => 'ADM_COMEX');
End;
/

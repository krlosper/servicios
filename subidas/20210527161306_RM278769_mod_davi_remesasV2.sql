set line 2000
set pagesize 2000
set serveroutput on size 999999
set linesize 1000
set feedback on
set trimspool on
set heading on
set serveroutput on

----------------------------------------
DECLARE 
SRV_MESSAGE         VARCHAR2(200);
IN_FLD_OPR_COR_EVE  NUMBER;
IN_FLD_OPR_FEC_EVE  VARCHAR2(200);
IN_FLD_OPR_COD_EVE  NUMBER;
IN_FLD_OPR_SUC_EVE  NUMBER;
IN_FLD_OPR_USR_EVE  VARCHAR2(200);
IN_FLD_OPR_TXT_EVE  VARCHAR2(200);
OUT_RESULT_MSG      VARCHAR2(200);
IN_XWSS_RESULT_SW   NUMBER;
IN_TRAN_COUNT       NUMBER;
xFLD_OPR_IDE_ORD    VARCHAR2(14);     
xMsg                Varchar2(250);

BEGIN 
SRV_MESSAGE         := NULL;
IN_FLD_OPR_COR_EVE  := '0';
IN_FLD_OPR_FEC_EVE  := SYSDATE;
IN_FLD_OPR_COD_EVE  := 998;--atdrevento
IN_FLD_OPR_SUC_EVE  := 7000;--acmxsucsal
IN_FLD_OPR_USR_EVE  := 'MCCALLEJ';
IN_FLD_OPR_TXT_EVE  := 'Proc Diario ModifEst a DEV RM259646';
OUT_RESULT_MSG      := NULL;
IN_XWSS_RESULT_SW   := NULL;
IN_TRAN_COUNT       := NULL;

    Delete  Tdummy
    where   Num = 20200605;
    Commit;

    Grabartdummy(20200605,'Inicia Proceso ['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');

    For Cur in (
        select distinct(tabla.FLD_OPR_NUM_ISN) FLD_OPR_NUM_ISN, tabla.FLD_OPR_REF_EXT FLD_OPR_REF_EXT
        from (
            SELECT  t.FLD_OPR_NUM_ISN FLD_OPR_NUM_ISN, t.FLD_OPR_REF_EXT FLD_OPR_REF_EXT, '1' Pos
            FROM    TDR T, OPREVETR O, OPRGLB G
            WHERE   T.FLD_M_32A_FVAL between to_date('2021/05/14','YYYY/MM/DD')  and to_date('2021/05/19','YYYY/MM/DD')
            AND     T.FLD_OPR_COD_EST IN ('APP','OBJ')
            and     ismoneylinea(t.FLD_OPR_MSG_TIPO) = 0
            AND     T.FLD_OPR_NUM_ISN  = G.FLD_OPR_NUM_ISN
            AND     G.FLD_OPR_IND_FPAG = '1' --Abono a Cuenta
            AND     T.FLD_OPR_NUM_ISN  = O.FLD_OPR_NUM_ISN  --Lavado
            AND     O.FLD_OPR_COD_EVE IN (SELECT COD_EVE FROM ATDREVENTO where eve_rel = 350)
            union
            SELECT  t.FLD_OPR_NUM_ISN FLD_OPR_NUM_ISN, t.FLD_OPR_REF_EXT FLD_OPR_REF_EXT, '2' Pos
            FROM    TDR T, OPREVETR O, OPRGLB G
            WHERE   T.FLD_M_32A_FVAL between to_date('2021/05/14','YYYY/MM/DD')  and to_date('2021/05/19','YYYY/MM/DD')
            AND     T.FLD_OPR_COD_EST IN ('APP','OBJ')
            and     ismoneylinea(t.FLD_OPR_MSG_TIPO) = 0
            AND     T.FLD_OPR_NUM_ISN  = G.FLD_OPR_NUM_ISN
            AND     G.FLD_OPR_IND_FPAG = '1' --Abono a Cuenta
            AND     T.FLD_OPR_NUM_ISN  = O.FLD_OPR_NUM_ISN
            AND     O.FLD_OPR_COD_EVE IN (114,115,116)--Fallo En Comunicacion Sun-One
            AND     (O.FLD_OPR_TXT_EVE LIKE '%TIPO DE CTA NO VALIDO%' OR O.FLD_OPR_TXT_EVE LIKE '%TRANSACCION NO PERMITIDA%')
            union
            SELECT  t.FLD_OPR_NUM_ISN FLD_OPR_NUM_ISN, t.FLD_OPR_REF_EXT FLD_OPR_REF_EXT, '3' Pos
            FROM    TDR T, OPREVETR O, OPRGLB G
            WHERE   T.FLD_M_32A_FVAL between to_date('2021/05/14','YYYY/MM/DD')  and to_date('2021/05/19','YYYY/MM/DD')
            AND     T.FLD_OPR_COD_EST IN ('APP','OBJ')
            and     ismoneylinea(t.FLD_OPR_MSG_TIPO) = 0
            AND     T.FLD_OPR_NUM_ISN  = G.FLD_OPR_NUM_ISN
            AND     G.FLD_OPR_IND_FPAG = '1' --Abono a Cuenta
            AND     T.FLD_OPR_NUM_ISN  = O.FLD_OPR_NUM_ISN
            AND     O.FLD_OPR_COD_EVE  = 99 --Cliente en Observacion
            AND     O.FLD_OPR_TXT_EVE LIKE '%Intento de Pago Automatico Cliente en Observacion%'
            union
            SELECT  t.FLD_OPR_NUM_ISN FLD_OPR_NUM_ISN, t.FLD_OPR_REF_EXT FLD_OPR_REF_EXT, '4' Pos
            FROM    TDR T, OPREVETR O, OPRGLB G
            WHERE   T.FLD_M_32A_FVAL between to_date('2021/05/14','YYYY/MM/DD')  and to_date('2021/05/19','YYYY/MM/DD')
            AND     T.FLD_OPR_COD_EST = 'OBJ'
            and     ismoneylinea(t.FLD_OPR_MSG_TIPO) = 0
            AND     T.FLD_OPR_NUM_ISN  = G.FLD_OPR_NUM_ISN
            AND     G.FLD_OPR_IND_FPAG = '1' --Abono a Cuenta
            AND     T.FLD_OPR_NUM_ISN  = O.FLD_OPR_NUM_ISN
            AND     O.FLD_OPR_COD_EVE  = 109--OBJETADA - LISTA RESTRICTIVA 
            AND     (O.FLD_OPR_TXT_EVE LIKE '%733%' OR O.FLD_OPR_TXT_EVE LIKE '%120%' OR O.FLD_OPR_TXT_EVE LIKE '%311%' OR O.FLD_OPR_TXT_EVE LIKE '%130%')) tabla ) Loop
    
        if trim(Cur.fld_opr_ref_ext) = '50639676510' then
            Grabartdummy(20200605,'Referencia No se debe Procesar ['||TRIM(Cur.fld_opr_num_isn)||']['||TRIM(Cur.fld_opr_ref_ext)||'] Proceso:['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');
            goto siguiente;
        end if;
   
        Begin    
            update  tdr
            set     FLD_OPR_COD_EST     = 'DEV'
                    ,FLD_OPR_NUM_AVI    = 999
            where   FLD_OPR_NUM_ISN     = Cur.fld_opr_num_isn;
            COMMIT;
        Exception When Others Then
            xMsg := substr(trim(sqlerrm),1,250); 
            Grabartdummy(20200605,'Error Act TDR:['||TRIM(Cur.fld_opr_num_isn)||'|'||xMsg||'] Proceso:['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');
            goto  siguiente;
        End;
            
        Begin
            TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve
                    (SRV_Message,
                    Cur.fld_opr_num_isn,
                    IN_FLD_OPR_COR_EVE,
                    IN_FLD_OPR_FEC_EVE,
                    IN_FLD_OPR_COD_EVE,
                    IN_FLD_OPR_SUC_EVE, 
                    IN_FLD_OPR_USR_EVE, 
                    IN_FLD_OPR_TXT_EVE, 
                    OUT_RESULT_MSG, 
                    IN_XWSS_RESULT_SW, 
                    IN_TRAN_COUNT );                
            COMMIT;
            
            if substr(trim(SRV_MESSAGE),1,1) <> '1' then
                xMsg := substr(trim(SRV_MESSAGE),1,250); 
                Grabartdummy(20200605,'1.Error Ingreso Evento TDR:['||TRIM(Cur.fld_opr_num_isn)||'|'||xMsg||'] Proceso:['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');        
                goto  siguiente;
            end if;
        Exception When Others Then
            xMsg := substr(trim(sqlerrm),1,250); 
            Grabartdummy(20200605,'2.Error Ingreso Evento TDR:['||TRIM(Cur.fld_opr_num_isn)||'|'||xMsg||'] Proceso:['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');    
            goto  siguiente;
        End;
        
        Grabartdummy(20200605,'Referencia Procesada OK:['||TRIM(Cur.fld_opr_num_isn)||']['||TRIM(Cur.fld_opr_ref_ext)||'] Proceso:['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');
<<siguiente>>
    null;
    End Loop;            
    Grabartdummy(20200605,'Finaliza Proceso ['||to_char(sysdate,'yyyy/mm/dd hh24:mi:ss')||']');
END; 
/
SELECT  TRIM(DESCRIP)
FROM    TDUMMY
WHERE   NUM = 20200605;
------------------------------------------------daviplata------------------------------------------------------
select TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS') AS F_INICIO
from dual;
Declare 
xNumeroDaviplata    varchar2(100);
tipID               varchar2(100);
numID               varchar2(100);
nomID               varchar2(100);
dirID               varchar2(100) := 'KR 13 No. 28-58';
ciuID               Varchar2(100) := 'BOGOTA D.C.';
codCiu              Number := 11001;
indCND              Varchar2(4) := '0000';
indOBS              Varchar2(1) := 'N';
indCLI              Varchar2(1) := 'U';
tipCli              Number := 0;
catTri              Number := 0;
tipCta              Number := 1;
existe              Number := 0;
davConsec           Number := 0;
numConvenio         Varchar2(16);
SRV_Message         Varchar2(500);
wss_result_sw       Number := 0;
Aux_TRAN_COUNT      Number := 0;
Aux_avi_remit       Number := 0;
--Registrar evento TDR
xNumCor             Number := 0;
Aux_opr_fec_eve     Date := sysdate;
Aux_cod_eve         Number := 0;
Aux_opr_cod_suc_rec Number := 33;
Aux_User            Varchar2(50) := 'ADM_COMEX';
Aux_opr_txt_eve     Varchar2(80);
wss_result_msg      Varchar2(500);
--Validacion Clinton
Aux_cli_vet         Number := 0;
Aux_llave_clint     varchar2(100);
xNomRem             Varchar2(80);
Aux_EsOfac          number := 0;
--Validacion rangos
AUX_R01_RANMAS      Number :=0;
AUX_R02_RANMAS      Number :=0;
-- Validacion tasas
Aux_opr_mto_pes     Number := 0;
Aux_opr_tasa        Number := 0;
xMtoPesos           Number := 0;
xDifPesos           Number := 0;
xAplica             Varchar2(1);
xDif                Number;
xPrd                Varchar2(4) := '0060';
Aux_fld_m_32a_fvta  Varchar2(10);
Retorna             Number := 0;
IdTasa              Number := 0;
--Validacion Lavado
indicadoresLAV      Number := 0;
Usuario             Varchar2(15) := 'TDR_SYSTEM';
EstadoFinal         Varchar2(4) := 'HLD';
Aux_opr_cod_est     Varchar2(3) := 'VAL';
--Proceso Pago
xTipoProc     Varchar2(2) := 'C';
xTranCount    Number := 0;
xResultMsg    Varchar2(80);
xcorEve       Number := 0;
xFecEve       Date := sysdate;
xCodEve       Number := 1598;
xSucEve       Number := 7000;
xUsuEve       Varchar2(50) := user;
xResultSW     Number := 0;
--Proceso PAGO
Aux_cmb_num_ope Varchar2(16);
xResult         Varchar2(1) := 'B';
xCodigoStatus   Number := 0;
xInd_Rev        Number :=0;
xMsg            Varchar2(255);
xEveFTP         Number := 0;
xGLS_TIPID      Varchar2(2);
xGLS_CODCMB     Varchar2(200) := 'Remesas de Trabajadores';
xNumTrs         Number :=0;
xDescError          ATDRDVLCOD.DescError%Type;
Begin
  Begin
      Select  SubStr(PSYS_P01,1,1), to_number(SubStr(PSYS_P01,3))
      Into    xAplica, xDif
      From    ADM_COMEX.ATDRPERSYS
      Where   PSYS_COD = 19;
  Exception When OTHERS Then xAplica := 'N'; xDif := 0;
  End;

  for x in (select fld_opr_ref_ext,fld_opr_num_isn,fld_opr_nom_ben As nomBen,FLD_OPR_MSG_TIPO,fld_opr_cta_cte_ben,fld_m_32a_fval,fld_m_32a_mto, fld_opr_cod_est,fld_opr_tip_ide,fld_opr_ide_ben,fld_opr_ide_ben_1,fld_opr_ide_ben_2,fld_opr_ide_ben_3,fld_opr_ide_ben_4,FLD_OPR_PAI_ORI_TDR,fld_opr_num_cga from ADM_COMEX.TDR where fld_opr_fec_cga >= to_date(sysdate-1,'YYYY/MM/DD')and fld_opr_msg_tipo = 'MNY' and fld_opr_cod_est in ('HLD','APP') and ltrim(fld_opr_ind_fpag,'0') = '5'  ) Loop
  
    select count(1)
    Into existe
    From ADM_COMEX.OPREVETR where fld_opr_num_isn = x.fld_opr_num_isn and fld_opr_cod_eve in (1503,1505); -- cliente no esta en C360/primer pago 

    if existe = 0 Then continue; End If;
    select fld_m_32z_tc,fld_m_32z_mto_pes,FLD_OPR_NOM_REM ,to_char(fld_m_32a_fvta,'YYYY/MM/DD'),fld_opr_msg_tipo
    INTO Aux_opr_tasa,Aux_opr_mto_pes,xNomRem ,Aux_fld_m_32a_fvta, x.FLD_OPR_MSG_TIPO
    FROM ADM_COMEX.OPRGLB 
    WHERE FLD_OPR_NUM_ISN  = x.fld_opr_num_isn;

--consultaDaviplata
    Begin
      Srv_Trans_BuS_Daviplata_pkg.Srv_Trans_BuS_Daviplata
            ( SRV_Message
            , x.fld_opr_cta_cte_ben -- Daviplata cliente  respuesta Stratus
            , Aux_opr_mto_pes
            , numConvenio           -- NumeroConvenio
            , xNumeroDaviplata      -- xNumeroDaviplata
            , tipID                 -- xTipoIdentificacion
            , numID                 -- xNroIdeDaviplata
            , nomID);               -- xNombreDaviplata
    Exception when others then 
      wss_result_sw := 9;  -- Error de Sistema
      wss_result_msg  := 'TDRsrv_existe_daviplata - (E03) Fallo Conexion SunOne.';
      SRV_Message     := '0780000' || wss_result_msg;
      TDRsrv_existe_daviplata_Pkg.EstadoHOLD(x.fld_opr_num_isn,x.fld_opr_cta_cte_ben,wss_result_sw,Aux_cod_eve); -- Fallo Conexion, no genera SMS x ahora 
      continue;
    End;

    if numID = 'NO EXISTE' THEN  --valida nro ide daviplata
      wss_result_sw := 2;  -- Daviplata no existe
      TDRsrv_existe_daviplata_Pkg.EstadoHOLD(x.fld_opr_num_isn,x.fld_opr_cta_cte_ben,wss_result_sw,Aux_cod_eve); -- NO EXISTE, si genera SMS (unico) 
      continue;
    ElsIf numID = 'FALLO CONEXION' Then
      wss_result_sw := 9;  -- Error de Sistema
      wss_result_msg  := 'TDRsrv_existe_daviplata - (E03) Fallo Conexion SunOne.';
      TDRsrv_existe_daviplata_Pkg.EstadoHOLD(x.fld_opr_num_isn,x.fld_opr_cta_cte_ben,wss_result_sw,Aux_cod_eve); -- Fallo Conexion, no genera SMS x ahora 
      continue;
    end if;

    if trim(tipID) is null THEN  -- valida Tipo nro ide daviplata
      wss_result_sw := 2;  -- Daviplata no existe
      TDRsrv_existe_daviplata_Pkg.EstadoHOLD(x.fld_opr_num_isn,x.fld_opr_cta_cte_ben,wss_result_sw,Aux_cod_eve); -- NO EXISTE, si genera SMS (unico)  
      continue;
    end if;

    If Trim(numID) is not null Then
      dirID   := 'KR 13 No. 28-58';
      ciuID   := 'BOGOTA D.C.';
      codCiu  := 11001;
      xSucEve := 7000;
      Begin
       select Nvl(Trim(fld_cln_dir1_cli),dirID) , 
              Nvl(Trim(fld_cln_ciu_cli),ciuID), 
              Case when Nvl(fld_cln_cod_suc,9999) in (0,9999) Then xSucEve Else fld_cln_cod_suc End,
              Case Nvl(FLD_CLN_COD_CIU,0) when 0 then codCiu else fld_cln_cod_ciu End
       Into dirID , ciuID, xSucEve, codCiu
       From ADM_COMEX.CLN 
       where fld_cln_cod_cli = LPad(numID,14,'0');
       Update CLN set fld_cln_dir1_cli = dirID,
                      fld_cln_ciu_cli  = ciuID,
                      fld_cln_cod_suc  = xSucEve,
                      fld_cln_cod_ciu  = codCiu,
                      fld_cln_fon_cli  = x.fld_opr_cta_cte_ben
       where fld_cln_cod_cli = LPad(numID,14,'0');
                      
      Exception when NO_DATA_FOUND Then
        dirID   := 'KR 13 No. 28-58';
        ciuID   := 'BOGOTA D.C.';
        codCiu  := 11001;
        xSucEve := 7000;
        insert into CLN (fld_cln_cod_cli,fld_cln_nom_cli,fld_cln_fon_cli,fld_cln_dir1_cli,fld_cln_ciu_cli,fld_cln_cod_suc,
                         FLD_CLN_NIT_CLI,FLD_CLN_FEC_ING_MOD,FLD_CLN_COD_TIP_ID,FLD_CLN_COD_CIU,
                         FLD_CLN_COD_NO_DES,FLD_CLN_IND_OBS,FLD_CLN_IND_TIPO)
        values (LPad(numID,14,'0'),nomID,x.fld_opr_cta_cte_ben,dirID,ciuID,xSucEve,LPad(numID,14,'0'),sysdate,tipID,codCiu,indCND,indOBS,indCLI);
      End;

      TDRsrv_upd_cta_tdr_Pkg.TDRsrv_upd_cta_tdr(SRV_Message, x.fld_opr_num_isn, x.fld_opr_cod_est,
                                                tipID, numID, tipCli, catTri, x.fld_opr_cta_cte_ben,
                                                tipCta, nomID, dirID, ciuID, x.fld_opr_cta_cte_ben,
                                                wss_result_sw, Aux_TRAN_COUNT );
      Aux_cod_eve     := 117;
      Aux_opr_txt_eve := SubStr('Se asigna Automatico CTA['||Trim(x.fld_opr_cta_cte_ben)||'] ID['||Trim(numID)||']',1,80);

      TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve(SRV_Message,x.fld_opr_num_isn,xNumCor,Aux_opr_fec_eve,
                                                Aux_cod_eve,Aux_opr_cod_suc_rec,Aux_User,Aux_opr_txt_eve,
                                                wss_result_msg,wss_result_sw,Aux_TRAN_COUNT) ;
      EstadoFinal := x.fld_opr_cod_est;
      if x.fld_opr_cod_est = 'HLD' Then -- Terminar de validar la transferencia
        Aux_opr_cod_est := 'VAL';
        EstadoFinal     := 'HLD';
        indicadoresLAV  := 0;
        Aux_cod_eve     := 0;
        Aux_opr_txt_eve := ' ';
        Aux_EsOfac      := 0;
<<CLINTON>>
        --Validacion lista clinton nombre Beneficiario
        TDRSrv_Verif_Clint_Pkg.TDRSrv_Verif_Clint(SRV_Message,x.nomBen, 0,Aux_cli_vet,Aux_llave_clint) ;
        If Aux_cli_vet > 0 then
          Aux_opr_cod_est := 'OBJ';
          Aux_cod_eve := 101;
          Aux_opr_txt_eve := 'NOMBRE BENEFICIARIO ['||Round(Aux_cli_vet,2)||'%] - '|| Aux_llave_clint;
          indicadoresLAV := 1;
          TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve(SRV_Message,x.fld_opr_num_isn,xNumCor,Aux_opr_fec_eve,
                                                    Aux_cod_eve,Aux_opr_cod_suc_rec,Aux_User,Aux_opr_txt_eve,
                                                    wss_result_msg,wss_result_sw,Aux_TRAN_COUNT) ;
        End If;

<<CLINTON_REM>>
        --Validacion lista clinton nombre Remitente
        TDRSrv_Verif_Clint_Pkg.TDRSrv_Verif_Clint(SRV_Message,xNomRem,1,Aux_cli_vet,Aux_llave_clint) ;
        If Aux_cli_vet > 0 then
          Aux_opr_cod_est := 'OBJ';
          Aux_cod_eve := 101;
          Aux_opr_txt_eve := 'NOMBRE REMITENTE ['||Round(Aux_cli_vet,2)||'%] - '|| Aux_llave_clint;
          indicadoresLAV := 1;
          TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve(SRV_Message,x.fld_opr_num_isn,xNumCor,Aux_opr_fec_eve,
                                                    Aux_cod_eve,Aux_opr_cod_suc_rec,Aux_User,Aux_opr_txt_eve,
                                                    wss_result_msg,wss_result_sw,Aux_TRAN_COUNT) ;
        End If;

<<ofac>>
        --11. validacion de Pais OFAC y GAFI
        TDRSrv_Verif_Ofac_Pkg.TDRSrv_Verif_Ofac(SRV_Message,x.FLD_OPR_PAI_ORI_TDR,x.FLD_OPR_MSG_TIPO, Aux_EsOfac ) ;
        If Aux_EsOfac = 1  Then
          Aux_opr_cod_est  := 'OBJ';
          Aux_cod_eve := 140;
          Aux_opr_txt_eve := 'Objetado por Pais OFAC.-'|| Fun_GlsPais(x.FLD_OPR_PAI_ORI_TDR);
          indicadoresLAV := 1;
          TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve(SRV_Message,x.fld_opr_num_isn,xNumCor,Aux_opr_fec_eve,
                                                    Aux_cod_eve,Aux_opr_cod_suc_rec,Aux_User,Aux_opr_txt_eve,
                                                    wss_result_msg,wss_result_sw,Aux_TRAN_COUNT) ;
        ElsIf Aux_EsOfac = 2 then
          Aux_opr_cod_est  := 'RCH';
          Aux_cod_eve := 240;
          Aux_avi_remit   :=  2;
          Aux_opr_txt_eve := 'Rechazada automaticamente por Pais GAFI.-'|| FUN_CONSULTAPAIS(x.fld_opr_pai_ori_tdr,5)||'('|| Fun_GlsPais(x.FLD_OPR_PAI_ORI_TDR)||')';
          indicadoresLAV := 1;
          TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve(SRV_Message,x.fld_opr_num_isn,xNumCor,Aux_opr_fec_eve,
                                                    Aux_cod_eve,Aux_opr_cod_suc_rec,Aux_User,Aux_opr_txt_eve,
                                                    wss_result_msg,wss_result_sw,Aux_TRAN_COUNT) ;
        End If;

<<RANGOS>>
        ---- VALIDA RANGOS DE LA TRANSFERENCIA
        Begin
          SELECT  R01_RANMAS, R02_RANMAS
          INTO    AUX_R01_RANMAS, AUX_R02_RANMAS
          FROM    ADM_COMEX.ATDRRANMAS
          WHERE   COD_RANMAS = X.FLD_OPR_MSG_TIPO;
        End;

        IF x.fld_m_32a_mto > AUX_R02_RANMAS And AUX_R02_RANMAS > 0 THEN
          Aux_opr_cod_est := 'RCH';
          Aux_cod_eve     := 205; --106; -- OBJETADA - MTO SUPERIOR A MAXIMO.
          Aux_opr_txt_eve := 'Valor Permitido '||formatNumber(AUX_R02_RANMAS,2);
          TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve(SRV_Message,x.fld_opr_num_isn,xNumCor,Aux_opr_fec_eve,
                                                    Aux_cod_eve,Aux_opr_cod_suc_rec,Aux_User,Aux_opr_txt_eve,
                                                    wss_result_msg,wss_result_sw,Aux_TRAN_COUNT) ;
        ELSIF x.fld_m_32a_mto < AUX_R01_RANMAS THEN
          Aux_opr_cod_est := 'RCH';
          Aux_cod_eve     := 205; --104; -- OBJETADA - MTO INFERIOR A MINIMO.
          Aux_opr_txt_eve := 'Valor Permitido '||formatNumber(AUX_R01_RANMAS,2);
          TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve(SRV_Message,x.fld_opr_num_isn,xNumCor,Aux_opr_fec_eve,
                                                    Aux_cod_eve,Aux_opr_cod_suc_rec,Aux_User,Aux_opr_txt_eve,
                                                    wss_result_msg,wss_result_sw,Aux_TRAN_COUNT) ;
        END IF;

<<TASAS>>
        --Valida Consistencia de tasa y montos
        xMtoPesos := x.fld_m_32a_mto * Aux_opr_tasa;
        xDifPesos := Abs(xMtoPesos - Aux_opr_mto_pes);

        if xAplica = 'N' Or xDifPesos > xDif Then -- Se rechaza
          Aux_cod_eve     := 208;
          Aux_opr_cod_est := 'RCH';
          Aux_avi_remit   :=  2;
          Aux_opr_txt_eve := 'Montos Inconsistentes. ['||x.fld_m_32a_mto||']*['||Aux_opr_tasa||'] vs ['||Aux_opr_mto_pes||']';
          TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve(SRV_Message,x.fld_opr_num_isn,xNumCor,Aux_opr_fec_eve,
                                                    Aux_cod_eve,Aux_opr_cod_suc_rec,Aux_User,Aux_opr_txt_eve,
                                                    wss_result_msg,wss_result_sw,Aux_TRAN_COUNT) ;
        End If;

        TDRSRV_EXE_VAL_OBJ_MT_PKG.ValidacionTasa(x.FLD_OPR_MSG_TIPO,Aux_fld_m_32a_fvta,
                                                 FUN_CONSULTAPAIS(to_char(x.FLD_OPR_PAI_ORI_TDR), 2),
                                                 xPrd, Aux_opr_tasa,IdTasa,Retorna);

        If Retorna = -1 Then
          SRV_Message := '0780000Error en TDRsrv_exe_val_obj_mt_Pkg no pudo consultar datos en la ACMXDESEMB';
        ElsIf Retorna = -2 Then
          Aux_cod_eve     := 207;
          Aux_opr_cod_est := 'RCH';
          Aux_avi_remit   :=  2;
          Aux_opr_txt_eve := 'La tasa no esta reportada para la fecha de la transferencia  ';
          TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve(SRV_Message,x.fld_opr_num_isn,xNumCor,Aux_opr_fec_eve,
                                                    Aux_cod_eve,Aux_opr_cod_suc_rec,Aux_User,Aux_opr_txt_eve,
                                                    wss_result_msg,wss_result_sw,Aux_TRAN_COUNT) ;
        ElsIf Retorna = 0 Then
          Aux_cod_eve     := 127;
          Aux_opr_txt_eve := 'Se asigna la tasa :'||to_char(IdTasa);
          TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve(SRV_Message,x.fld_opr_num_isn,xNumCor,Aux_opr_fec_eve,
                                                    Aux_cod_eve,Aux_opr_cod_suc_rec,Aux_User,Aux_opr_txt_eve,
                                                    wss_result_msg,wss_result_sw,Aux_TRAN_COUNT) ;
        End If;

<<INDICADORES>>
        -----   objeciones por indicadores de lavado de activos

        If indicadoresLAV = 0 Then
          TDRsrv_exe_val_ind_lav_Pkg.TDRsrv_exe_val_ind_lav( SRV_Message, x.fld_opr_num_isn, Aux_opr_cod_est, Usuario);
        End If;

        UPDATE  TDR
        SET     FLD_OPR_IND_FPAG = '5'
                ,FLD_OPR_CTA_CTE_BEN = x.fld_opr_cta_cte_ben
                ,FLD_OPR_TIP_CTA_CTE = 1
                ,fld_opr_pri_avi = case Aux_cod_eve When 1503 Then 5 Else fld_opr_pri_avi End
                ,FLD_OPR_IND_VAL = 0
        WHERE   FLD_OPR_NUM_ISN = x.fld_opr_num_isn;

        EstadoFinal := Aux_opr_cod_est;

        ---Actualizacion estado en TDR
        TDRsrv_upd_est_glb_Pkg.TDRsrv_upd_est_glb(SRV_Message,x.fld_opr_num_isn,Aux_opr_cod_est,nvl(Aux_avi_remit,0),wss_result_sw,Aux_TRAN_COUNT) ;
    
-----------
        If EstadoFinal = 'VAL' Then
            xTranCount := 0;
            Begin
              Srv_Opr_Conexg_Ope_Pkg.Srv_Opr_Conexg_Ope(Srv_Message,
                                                        x.fld_opr_num_isn,
                                                        xTipoProc,
                                                        x.fld_opr_num_cga,
                                                        xTranCount,
                                                        xResultMsg);
              Exception When Others Then SRV_Message := '0'||sqlerrm;
            End;                                                         
            If Substr(SRV_Message, 1, 1) = '0' Then
                xResultMsg := Substr(Srv_Message,8,80);
                xTranCount := 0;
                tdrsrv_put_opr_eve_Pkg.tdrsrv_put_opr_eve(SRV_Message
                                                          , x.Fld_Opr_Num_Isn
                                                          , xCorEve 
                                                          , xFecEve
                                                          , xCodEve -- 1598  
                                                          , xSucEve
                                                          , xUsuEve
                                                          , xResultMsg
                                                          , xResultMsg
                                                          , xResultSW
                                                          , xTranCount);
            Else EstadoFinal := 'APP';
            End If;
        End If;
      End if;  -- el estado

      if EstadoFinal = 'APP' Then
          Aux_opr_cod_est := 'PPG';
          Aux_avi_remit := 0;
          TDRsrv_upd_est_glb_Pkg.TDRsrv_upd_est_glb(SRV_Message,x.fld_opr_num_isn,Aux_opr_cod_est,nvl(Aux_avi_remit,0),wss_result_sw,Aux_TRAN_COUNT) ;

          --Generar Operacion De Cambio
          Aux_TRAN_COUNT := 0;
          TDRsrv_gra_ope_cmb_Pkg.TDRsrv_gra_ope_cmb(SRV_Message,x.fld_opr_num_isn, Aux_cmb_num_ope, Aux_TRAN_COUNT) ;

          Update  TDR
          set fld_opr_ind_fpag = '5'
             ,fld_opr_cod_suc_pag = 60
          Where fld_opr_num_isn = x.fld_opr_num_isn;

          Update  OPE
          set fld_cmb_cod_suc = 60
             ,fld_cmb_suc_pag = 60
          where fld_cmb_num_ope = Aux_cmb_num_ope;

          --Llamar Trama De Pago Automatico
          xInd_Rev := 0;
          xEveFTP  := 0;

          SvcLogPagAut_Pkg.SvcLogPagAut(x.fld_opr_num_isn,Aux_opr_mto_pes,'Inicia 7524 '||Trim(x.fld_opr_cta_cte_ben)||'|'||numConvenio);
          Srv_Trans_7524_Pkg.Srv_Trans_7524(SRV_Message,x.fld_opr_cta_cte_ben,numConvenio,Aux_cmb_num_ope,Aux_opr_mto_pes,tipID,
                                            numID,xResult,xCodigoStatus);

<<Respuesta>>
          If Trim(xResult) <> 'B' Then  --Si No Se Pudo Realizar Pago Automatico
            If xCodigoStatus = 0 Then xInd_Rev :=1; End If; -- Queda Ppg Para Q Sea Desbloqueada O Pagada Segun Fondos Aplicados
        -- Oprevetr Se Ingresa Evento De Intento De Pago Automatico (Evento = 116): Con Texto = Substr(Srv_Message,7,80)
            xNumCor         := 0;
            Aux_opr_fec_eve := Sysdate;
            Aux_opr_txt_eve := 'Fallo: '||Substr(SRV_Message,8,73);
            xMsg := 'Fallo: '||Substr(SRV_Message,8,73);
            SRV_Message := '1000000';
            Aux_cod_eve         := 116;
            Aux_opr_cod_suc_rec := 60;

            TDRsrv_put_opr_eve_Pkg.TDRsrv_put_opr_eve
               ( SRV_Message,
                 x.fld_opr_num_isn,
                 xNumCor,
                 Aux_opr_fec_eve,
                 Aux_cod_eve,
                 Aux_opr_cod_suc_rec,
                 Usuario,
                 Aux_opr_txt_eve,
                 wss_result_msg,
                 wss_result_sw,
                 Aux_TRAN_COUNT) ;

            Update  OPREVETR
            Set FLD_OPR_MSG_GEN = '1'
            Where fld_opr_num_isn = x.fld_opr_num_isn
              And fld_opr_cod_eve = Aux_cod_eve;
            
            --Reversa La Operacion De Cambio

            If  xCodigoStatus > 0 Then -- Se Verifica Codigo Status Recibido
             Begin
                Select Cod_Eve, DescError
                Into    xEveFTP,xDescError
                From    ADM_COMEX.ATDRDVLCOD
                where   CodigoError = xCodigoStatus;
             Exception When Others Then xEveFTP := 0;
             End;

             If xEveFTP > 0 Then
               Aux_opr_txt_eve := To_Char(xCodigoStatus) || ' - ' || xDescError;
               TDRSrv_exe_pga_mdt_Pkg.GenerarEventoFTP(x.fld_opr_num_isn,xEveFTP,Aux_opr_txt_eve);
               xInd_Rev := 0;
             elsif xCodigoStatus = 2103 then xInd_Rev := 1; -- TimeOut deja la transferencia en PPG y no reintenta pago.
             Else
                TDRSrv_exe_pga_mdt_Pkg.RegistrarReintentoPago(x.fld_opr_cta_cte_ben,
                                       numConvenio,
                                       Aux_cmb_num_ope,
                                       Aux_opr_mto_pes,
                                       tipID,
                                       numID,
                                       x.fld_opr_num_isn,
                                       x.fld_opr_ref_ext,
                                       nomID,
                                       12);
                xInd_Rev := 1;
             End If;
            End If;
            if xIND_Rev = 0 Then
             Aux_TRAN_COUNT := 0;
             TDRsrv_desb_ppg_Pkg.TDRsrv_desb_ppg(SRV_Message,x.fld_opr_num_isn,Aux_TRAN_COUNT);
            End If;
            ---Actualiza Campos En La Tdr

            UPDATE  TDR
            SET FLD_OPR_OBS_REG = Usuario || ' NO PUDO REALIZAR PAGO AUTOMATICO ' || To_Char(Sysdate,'YYYY/MM/DD HH24:MI:SS')
               ,FLD_OPR_TIP_CLI    = 1
            WHERE   TDR.FLD_OPR_NUM_ISN = x.fld_opr_num_isn;

            SvcLogPagAut_Pkg.SvcLogPagAut(x.fld_opr_num_isn,Aux_opr_mto_pes,'FIN PAGO AUT NO EXITOSO '||xMsg);
          ElsIf Trim(xResult) = 'B' Then   --Pago Automatico Ok
            SvcLogPagAut_Pkg.SvcLogPagAut(x.fld_opr_num_isn,Aux_opr_mto_pes,'FIN PAGO AUTOMATICO EXITOSO');
            --Realiza Timbre De Caja Para Pago Automatico Y El Codigo De Evento Es 993
            update  OPE
            set  fld_cmb_suc_pag = 60
                ,fld_cmb_usr_pag = Case Usuario When 'TDR_SYSTEM' Then 'AUTOMATICO' Else Usuario End
                ,FLD_PUC_R10C13_NUM_CMB = 1809
                , fld_cmb_cls_cot  = 4
            where   fld_cmb_num_ope = Aux_cmb_num_ope;

            Aux_cod_eve := 993;
            Aux_TRAN_COUNT := 0;
            TDRsrv_timbre_caja_Pkg.TDRsrv_timbre_caja(SRV_Message,Aux_cmb_num_ope,Aux_cod_eve,60,Aux_TRAN_COUNT) ;

            xGLS_TIPID := Fun_TipoID_BR(numID);
            Insert into OPEADI (fld_cmb_num_ope,
                                Formato,
                                ReferenciaInterna,
                                ReferenciaExterna,
                                Origen,
                                MonedaISO,
                                FormaPago,
                                Formulario,
                                TipoID,
                                NombreCln,NOMBRETIPOID, CUENTA, NUMERAL, FLD_TRS_NUM_TRS, IND_ABOAUT)
            Values (Aux_cmb_num_ope,
                    '2:Pago Automatico',
                    x.fld_opr_num_isn,
                    x.fld_opr_ref_ext,
                    Cod_Otr(x.fld_opr_num_isn),
                    'USD',
                    'CTO',
                    '5',
                    tipID,
                    nomID,xGLS_TIPID,x.fld_opr_cta_cte_ben,xGLS_CODCMB, xNumTrs,4);  -- Abono Automatico
           End If; -- Result del Pago
      End If; --estado APP
    End If; --Identificacion valida  
  End Loop;
  commit;
End;
/
select TO_CHAR(SYSDATE,'YYYY/MM/DD HH24:MI:SS') AS F_FIN
from dual;
exit
  

